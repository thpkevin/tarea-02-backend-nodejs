// path - ruta : api/users
import { Router } from 'express';
import { createFood } from '../controllers/food.controller';

const router = Router();

router.post('/', createFood);

export default router;