import Food from '../models/food.model';


const createFood = async (request, response) => {
    try{
        const {name,calories} = request.body;
        if(name && calories){
            const food = new Food(request.body);
            await food.save();
            response.status(201).send({
                success: true,
                message: 'Comida creado correctamente'
            })
            return;
        }
        return response.status(400).send({
            sucess: false,
            message: 'Datos invalidos'
        })
    }catch(error){
        response.status(500).send({
            error,
            sucess: false,
            message: 'Ha ocurrido un problema'
        })
    }

}


export { createFood }