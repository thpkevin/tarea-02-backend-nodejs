import { Schema, model } from 'mongoose';

const FoodSchema = new Schema({
    name: {
        type: String,
        required: true,
        uppercase: true
    },
    calories: {
        type: Number,
        required: true
    },
	status: {
		type: Boolean,
		required: false,
        default: true
	},
    created: {
        type: Date,
        default: Date.now
    }    
});

export default model('foods', FoodSchema);


